const csvToJSON=require('../csv-json');

const writingFile=require('fs');

function noOfTimesTeamWonBothTossAndMatch(){
    csvToJSON('../data/matches.csv').then((matchesData)=>{
        let teamsWinsBothTossAndMatch ={};
        try{
            for(let data of matchesData){
                if(data.toss_winner===data.winner){
                    if(data.winner in teamsWinsBothTossAndMatch){
                        teamsWinsBothTossAndMatch[data.winner]=teamsWinsBothTossAndMatch[data.winner]+1;
                    }else{
                        teamsWinsBothTossAndMatch[data.winner]=1;
                    }

                }
            };

        }catch(error){

        }

        writingFile.writeFile('../public/output/5-teams-wins-both-toss-and-match.json',JSON.stringify(teamsWinsBothTossAndMatch),(err)=>{
            if(err){
                console.log(err);
            }else{
                console.log("Successfully completed ....");
            }
        })
    })
}


console.log(noOfTimesTeamWonBothTossAndMatch());