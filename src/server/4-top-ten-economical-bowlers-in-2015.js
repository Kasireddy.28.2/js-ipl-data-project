const csvToJSON=require("../csv-json") ;

const writingFile=require("fs");

function topTenEconomicalBowlers(){
    csvToJSON("../data/matches.csv").then((matchesData)=>{
        let match_id=[];
        try{
            for(let data of matchesData){
                if(data.season==="2015"){
                    match_id.push(data.id);
                }
            }

        }catch(error){
            console.log(error);
        };

        csvToJSON("../data/deliveries.csv").then((deliveriesData)=>{
            let economicalBowlersData={};
            try{
                for(let id of match_id){
                    for (let matchDelivery of deliveriesData) {
                        if (id === matchDelivery.match_id) {
                            let bowler = matchDelivery.bowler;
                            let runsConcededByBowler =Number(matchDelivery.total_runs) -  Number(matchDelivery.bye_runs) - Number(matchDelivery.legbye_runs);

         
                        if (!economicalBowlersData[bowler]) {
                            economicalBowlersData[bowler] = {};
                            economicalBowlersData[bowler]["runs"]=0;
                            economicalBowlersData[bowler]["balls"]=0;
                        }
         
                            economicalBowlersData[bowler].runs += runsConcededByBowler;
                            economicalBowlersData[bowler].balls += 1;
                        }
                   }
                };

            }catch(error){
                console.log(error);
            }

            let economyOfBowler={};
                for(let bowler in economicalBowlersData){
                    economyOfBowler[bowler]={};
                    economyOfBowler[bowler]=(economicalBowlersData[bowler].runs/(economicalBowlersData[bowler].balls/6)).toFixed(2);
                    
                }

                let economicalBowlers=[];
                for(let data in economyOfBowler){
                    economicalBowlers.push([data,economyOfBowler[data]]);
                }

                //console.log(economicalBowlers);
                let sortingBowlers=economicalBowlers.sort((a,b)=>{
                    return a[1]-b[1];
                })
                
                let topTenEconomicalBowlersData=[];

                for(let index=0;index<10;index++){
                    topTenEconomicalBowlersData.push(sortingBowlers[index]);
                }

                let result={};

                for(let bowlers of topTenEconomicalBowlersData){
                    let bowler=bowlers[0];
                    let economy=bowlers[1];
                    if(!(bowlers in result)){
                        result[bowler]=economy;

                    }

                };
                
                writingFile.writeFile("../public/output/4-top-10-economical-bowlers.json",JSON.stringify(result),(error)=>{
                if(error){
                    console.log(error);
                }else{
                    console.log("Successfully completed ....");
                }
            })

            
            
            
        })
        
    })

};


topTenEconomicalBowlers();