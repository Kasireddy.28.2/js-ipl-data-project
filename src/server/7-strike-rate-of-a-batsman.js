const csvToJSON = require("../csv-json");

const writingFile = require("fs");

function strikeRateOfBatsman() {
    csvToJSON("../data/matches.csv").then((matchesData) => {

        csvToJSON("../data/deliveries.csv").then((deliveriesData) => {
            let batsmanData = {};
            try{
                for (let matches of matchesData) {
                    for (let deliveries of deliveriesData) {
                        if (matches.id === deliveries.match_id) {
                            if (batsmanData[matches.season] == undefined) {
                                batsmanData[matches.season] = {}
                            }
    
                            if (deliveries.batsman in batsmanData[matches.season]) {
                                batsmanData[matches.season][deliveries.batsman].runs += Number(deliveries.batsman_runs);
                                batsmanData[matches.season][deliveries.batsman].balls += 1
                            } else {
                                batsmanData[matches.season][deliveries.batsman] = { runs: Number(deliveries.batsman_runs), balls: 1}
                            }
    
                        }
                    }
                }
            }catch(error){
                console.log(error);
            }

            let strikeRateOfBatsman={};

            try{
                
                for(let season in batsmanData){
                    for(let batsman in batsmanData[season]){
                        if(strikeRateOfBatsman[season]=== undefined){
                            strikeRateOfBatsman[season]={}
                        }
    
                        strikeRateOfBatsman[season][batsman]=Number(((batsmanData[season][batsman].runs/batsmanData[season][batsman].balls)*100).toFixed(2));
                    }
                    
                }
            }catch(error){
                console.log(error);
            }

            writingFile.writeFile("../public/output/7-strike-rate-of-batsman.json",JSON.stringify(strikeRateOfBatsman),(error)=>{
                if(error){
                    console.log(error);
                }else{
                    console.log(`Successfully completed ....`);
                }
            })


            
        })
    })


};
strikeRateOfBatsman();