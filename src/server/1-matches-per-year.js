const csvToJson=require("../csv-json");

const writingFile=require("fs");


function matchesPerYear(){
    csvToJson("../data/matches.csv").then((data)=>{
        let numberOfMatches={};
        try{
            for(let element of data){
                if(element.season in numberOfMatches){
                    numberOfMatches[element.season]=numberOfMatches[element.season]+1;
                }else{
                    numberOfMatches[element.season]=1;
                }
            }
            console.log(numberOfMatches);
            
        }catch(error){
            console.log(error);
        }
        

        writingFile.writeFile("../public/output/1-matches-per-year.json",JSON.stringify(numberOfMatches),(err)=>{
            if(err){
                console.log(err);
            }else{
                console.log("Successfully completed ....");
            }
        })
    })
}


matchesPerYear();
