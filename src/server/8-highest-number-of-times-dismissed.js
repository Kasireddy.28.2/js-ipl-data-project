const csvToJSON=require("../csv-json");

const writingFile=require("fs");

function highestNumberOfTimesDismissal(){
    csvToJSON("../data/deliveries.csv").then((deliveriesData)=>{
        let dismisals={};
        try{
        for(let data of deliveriesData){
            let playerDismissed=data.player_dismissed;
            let bowler=data.bowler;

            if(playerDismissed!==''){
                if(playerDismissed in dismisals){
                    if(bowler in dismisals[playerDismissed]){
                        dismisals[playerDismissed][bowler]+=1
                    }else{
                        dismisals[playerDismissed][bowler]=1
                    }

                }else{
                    dismisals[playerDismissed]={};
                    dismisals[playerDismissed][bowler]=1;
                    

                }

            }
            
        }
    }catch(error){
        console.log(error);
    }
    let highest={};
    try{
    for(let batsman in dismisals){
            let s=0
            for(let bowler in dismisals[batsman] ){
                if(dismisals[batsman][bowler]>s){
                    s=dismisals[batsman][bowler];
                    highest[batsman]={};
                    highest[batsman][bowler]=dismisals[batsman][bowler];
                }
            }

        }
    }catch(error){
        console.log(error);
    }
        let topDismissals={batsman:undefined,bowler:undefined,dismisals:undefined};

        try{
        let n=0;
        for(let batsman in highest){
            for(let bowler in highest[batsman]){
                if(highest[batsman][bowler]>n){
                    n=highest[batsman][bowler];
                    topDismissals.batsman=batsman;
                    topDismissals.bowler=bowler;
                    topDismissals.dismisals=highest[batsman][bowler];
                    
                }
            }
        };

        console.log(topDismissals);
    }catch(error){
        console.log(error);
    }

        writingFile.writeFile("../public/output/8-highest-no-of-times-player-dismissed.json",JSON.stringify(topDismissals),(error)=>{
            if(error){
                console.log(error);
            }else{
                console.log("Successfully completed .....");
            }
        })
    })
}


highestNumberOfTimesDismissal();