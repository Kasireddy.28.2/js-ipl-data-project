const csvToJSON = require('../csv-json');
const writingFile = require('fs');

function extraRunsByTeam(){
    csvToJSON('../data/matches.csv').then((matchesData)=>{
        let runs_conceded_by_team={};
        let match_ids=[];
        try{
            for(let element of matchesData){
                if(element.season==="2016"){
                    match_ids.push(element.id);
                }
            }

        }catch(error){
            console.log(error);
        }
        


        csvToJSON('../data/deliveries.csv').then((deliveriesData)=>{

            try{
                for(let id of match_ids){
                    for(let element of deliveriesData){
                        if(id===element.match_id){
                            if(element.bowling_team in runs_conceded_by_team){
                                runs_conceded_by_team[element.bowling_team]=runs_conceded_by_team[element.bowling_team]+Number(element.extra_runs);
                            }else{
                                runs_conceded_by_team[element.bowling_team]=Number(element.extra_runs);
                            }
                        }
                    }
                }

            }catch(error){
                console.log(error);
            }
             

            writingFile.writeFile('../public/output/3-runs-conceded-per-team-in-2016.json',JSON.stringify(runs_conceded_by_team),(err)=>{
                if(err){
                    console.log(err);
                }else{
                    console.log("Successfully completed ....");
                }
            })


      })



    })

    
    
};

console.log(extraRunsByTeam());