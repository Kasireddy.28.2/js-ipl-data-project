const csvToJSON=require("../csv-json");

const writingFile=require("fs");

function bestEconomyBowlerInSuperOver(){
    csvToJSON("../data/deliveries.csv").then((deliveriesData)=>{
        let bowlersData={};
        try{
        for(let matchDelivery of deliveriesData){
            if(matchDelivery.is_super_over=="1"){
                let runsInSuperOver=Number(matchDelivery.total_runs)-Number(matchDelivery.bye_runs)-Number(matchDelivery.legbye_runs);
                if(!bowlersData[matchDelivery.bowler]){
                    bowlersData[matchDelivery.bowler]={};
                    bowlersData[matchDelivery.bowler]={
                        runs:0,
                        balls:0
                    }
                }

                bowlersData[matchDelivery.bowler].runs+=runsInSuperOver;
                bowlersData[matchDelivery.bowler].balls+=1
                
            }

        }
    }catch(error){
        console.log(error);
    }

        let bowlerEconomy={};
        try{
        for(let bowler in bowlersData){
            bowlerEconomy[bowler]={economy:(bowlersData[bowler].runs/(bowlersData[bowler].balls/6)).toFixed(2)};
        }
    }catch(error){
        console.log(error);
    }

        let arrayEconomyBowler=[];
        try{
        for(let bowler in bowlerEconomy){
            arrayEconomyBowler.push([bowler,bowlerEconomy[bowler]]);
        }
    }catch(error){
        console.log(error);
    }

         let sortingBowlers=arrayEconomyBowler.sort((a,b)=>{
            return a[1].economy-b[1].economy
         });
        
        let topEconomicalBowlerInSuperOver=sortingBowlers.splice(0,1);
        let result={bowler:undefined,economy:undefined};
        result.bowler=topEconomicalBowlerInSuperOver[0][0];
        result.economy=topEconomicalBowlerInSuperOver[0][1].economy;


        writingFile.writeFile("../public/output/9-top-economical-bowler-in-super-over.json",JSON.stringify(result),(error)=>{
            if(error){
                console.log(error);
            }else{
                console.log("Successfully completed ....");
            }
        })



    })
}

bestEconomyBowlerInSuperOver();