const csvToJSON=require("../csv-json");
const writingFile=require('fs');

function highestNoOfPlayerOfTheMatchAwards(){
    csvToJSON("../data/matches.csv").then((matchesData)=>{
        let obj={};
        try{
        for(let data of matchesData){
            if(data.season in obj){
                if(data.player_of_match in obj[data.season]){
                    obj[data.season][data.player_of_match]=obj[data.season][data.player_of_match]+1;
                }else{
                    obj[data.season][data.player_of_match]=1;
                }

            }else{
                obj[data.season]={};
                obj[data.season][data.player_of_match]=1;
            }
        }
    }catch(error){
        console.log(error);
    }
        let players={};
        try{
        for(let data in obj){
            let winning_awards=0;
            for(let player in obj[data]){
                if(obj[data][player]>winning_awards){
                    players[data]=player;
                    winning_awards=obj[data][player];
                }
            }    

        }
    }catch(error){
        console.log(error);
    }

        writingFile.writeFile('../public/output/6-highest-number-of-player-of-match-award.json',JSON.stringify(players),(err)=>{
            if(err){
                console.log(err);
            }else{
                console.log("Problem Completed Successfully");
            }
        })
    })
}


highestNoOfPlayerOfTheMatchAwards();