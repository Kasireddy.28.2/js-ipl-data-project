const csvToJSON = require('../csv-json');
const writingFile = require('fs');

function matchesWonPerYearPerTeam(){
    csvToJSON('../data/matches.csv').then((matchesData) => {
        let matchesWonPerTeamPerYear={};
        try{
            for (let data of matchesData){
                if(data.season in matchesWonPerTeamPerYear){
                    if(data.winner in matchesWonPerTeamPerYear[data.season]){
                        matchesWonPerTeamPerYear[data.season][data.winner]=matchesWonPerTeamPerYear[data.season][data.winner]+1
    
                     }else{
                        matchesWonPerTeamPerYear[data.season][data.winner]=1;
                     }
                }else{
                    matchesWonPerTeamPerYear[data.season]={};
                    matchesWonPerTeamPerYear[data.season][data.winner]=1;
    
                }
            }
    
        }catch(error){
            console.log(error);
        }
        
        
        writingFile.writeFile('../public/output/2-matches-won-per-year-per-team.json', JSON.stringify(matchesWonPerTeamPerYear) , (err) => {
            if(err){
                console.log(err);
            }else{
                console.log('Successfully completed ...');
            }
        })
    })
};

matchesWonPerYearPerTeam();
